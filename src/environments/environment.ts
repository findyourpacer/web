// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

// export const environment = {
//   production: false,
//   firebase: {
//     apiKey: "AIzaSyDJLEjXt0eZ6WkH_CMvbosYMrkZsC90p5M",
//     authDomain: "fyp-manager-c9b8b.firebaseapp.com",
//     databaseURL: "https://fyp-manager-c9b8b.firebaseio.com",
//     projectId: "fyp-manager-c9b8b",
//     storageBucket: "",
//     messagingSenderId: "1039941117137",
//     appId: "1:1039941117137:web:5aa4c8a12450a0d66d91f0",
//     measurementId: "G-DFVNMF60W8"
//   }
// };

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAkK0MdW-6rWbpkEqCT3BitEMf_XiR9UEE",
    authDomain: "findyourpacer.firebaseapp.com",
    databaseURL: "https://findyourpacer.firebaseio.com",
    projectId: "findyourpacer",
    storageBucket: "findyourpacer.appspot.com",
    messagingSenderId: "839232801850",
    appId: "1:839232801850:web:de2503c0a5d5222ae13903",
    measurementId: "G-2Z0G6G0SW3"
  }
};

export const fcmKey = {
  key: "AIzaSyDK4Lrl38wuRScnJbE-z7eXxdP4JeCcSko"
}