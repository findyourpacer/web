import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSmartModalComponent, NgxSmartModalService } from 'ngx-smart-modal';
import { Subscription } from 'rxjs';
import { isString } from 'util';
import { AudictModel } from '../../models/audict.model';
import { EventModel } from '../../models/event.model';
import { EventService } from '../../services/event.service';

@Component({
  selector: 'app-event',
  templateUrl: 'event.component.html',
  styleUrls: ['event.component.scss']
})
export class EventComponent implements OnInit {
  private eventsSubscription: Subscription;

  eventForm: FormGroup;
  modalities: FormArray;
  times: FormArray;
  event: EventModel;
  audict: AudictModel;
  events: Array<EventModel>;
  key: string;
  uploadProgress: any;
  uploadedBanner: string;
  startedUpload: boolean;
  viewMode: boolean;

  constructor(
    private eventService: EventService,
    private db: AngularFirestore,
    public ngxSmartModalService: NgxSmartModalService,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.createForm();
    this.eventsSubscription = this.eventService.getAllEvents().subscribe(data => {
      this.events = data.filter(it => it.status !== 'DELETED');
    });
  }

  ngOnInit(): void {
    this.event = new EventModel();
    this.event.audict = new AudictModel();
  }

  ngOnDestroy(): void {
    this.eventsSubscription.unsubscribe();
  }

  createForm() {
    this.eventForm = this.fb.group({
      // id: [''],
      name: ['', Validators.required],
      date: ['', Validators.required],
      place: this.fb.group({
        country: [''],
        state: [''],
        city: [''],
        coordinate: ['']
      }),
      link: [''],
      modalities: this.fb.array([this.createModality()]),
      banner: [''],
      registerLink: [''],
      registerPeriod: this.fb.group({
        begin: [''],
        end: ['']
      }),
      status: [''],
      audict: this.fb.group({
        createdAt: [''],
        createdBy: [''],
        updatedAt: [''],
        updatedBy: [''],
        deletedAt: [''],
        deletedBy: ['']
      }),
    });
  }

  createModality(): FormGroup {
    return this.fb.group({
      name: [''],
      distance: [''],
      gender: [''],
      times: this.fb.array(['03:30 - 04:00', '04:00 - 04:30', '04:30 - 05:00', '05:00 - 05:30', '05:30 - 06:00', '06:00 - 06:30', '06:30 - 07:00', '07:00+']),
      // times: this.fb.array([this.createTime()]),
    });
  }

  addModality(): void {
    this.modalities = this.eventForm.get('modalities') as FormArray;
    this.modalities.push(this.createModality());
  }

  removeModality(index): void {
    this.modalities = this.eventForm.get('modalities') as FormArray;
    this.modalities.removeAt(index);
  }

  createTime(): FormControl {
    return this.fb.control('');
  }

  addTime(modality) {
    this.times = (<FormArray>this.eventForm.get('modalities')).at(modality).get('times') as FormArray;
    this.times.push(this.createTime());
  }
  removeTime(modality, index): void {
    this.times = (<FormArray>this.eventForm.get('modalities')).at(modality).get('times') as FormArray;
    this.times.removeAt(index);
  }

  onSubmit() {
    if (this.eventForm.valid) {
      this.event = this.eventForm.value as EventModel;
      this.event.date = this.eventForm.get('date').value != '' ? new Date(this.eventForm.get('date').value) : null;
      // this.event.date.setMinutes( this.event.date.getMinutes() + this.event.date.getTimezoneOffset() );
      this.event.registerPeriod.begin = this.eventForm.get('registerPeriod').get('begin').value !== '' ? new Date(this.eventForm.get('registerPeriod').get('begin').value) : null;
      this.event.registerPeriod.end = this.eventForm.get('registerPeriod').get('end').value !== '' ? new Date(this.eventForm.get('registerPeriod').get('end').value) : null;

      this.event.banner = isString(this.uploadedBanner) ? this.uploadedBanner : '';
      if (this.key) {
        this.eventService.update(this.event, this.key);
      } else {
        this.eventService.insert(this.event);
      }

      this.event = new EventModel();
      this.eventForm.reset();
      this.key = null;
      this.createForm();
      this.uploadedBanner = null;
      this.ngxSmartModalService.getModal('createEventModal').close();
    } else {
      return this.eventForm.errors;
    }
  }

  upload(event) {
    this.startedUpload = true;
    this.eventService.uploadImage('event_banners', event.target.files[0]).then(it => {
      this.uploadedBanner = <string>it;
      this.startedUpload = false;
    });
  }
  removeBanner() {
    this.uploadedBanner = null;
  }

  delete(key: string) {
    this.eventService.getEvent(key).subscribe(it => {
      this.event = it as EventModel;
      this.event.status = 'DELETED';
      this.event.audict.deletedAt = new Date();
      this.event.audict.deletedBy = this.eventService.getUID();
      this.eventService.update(this.event, key);
    });
    // this.eventService.delete(key);
  }

  // deletevdd(key: string) {
  //   console.log(key);
  //   this.eventService.delete(key);
  // }

  bindFormToModel(it) {
    this.eventForm.patchValue({
      name: it['name'],
      // date: it['date'].seconds > 0 ? new Date(it['date'].toMillis()).toISOString().substring(0, 10) : null,
      date: it['date'].seconds > 0 ? new Date(it['date'].seconds*1000) : null,
      place: {
        country: it['place']['country'],
        state: it['place']['state'],
        city: it['place']['city'],
        coordinate: it['place']['coordinate']
      },
      link: it['link'],
      registerLink: it['registerLink'],
      registerPeriod: {
        begin: it['registerPeriod']['begin'] != null ? new Date(it['registerPeriod']['begin'].toMillis()).toISOString().substring(0, 10) : null,
        end: it['registerPeriod']['end'] != null ? new Date(it['registerPeriod']['end'].toMillis()).toISOString().substring(0, 10) : null
      },
      status: it['status'],
      audict: {
        createdAt: it['audict']['createdAt'],
        createdBy: it['audict']['createdBy'],
        updatedAt: it['audict']['updatedAt'],
        updatedBy: it['audict']['updatedBy'],
        deletedAt: it['audict']['deletedAt'],
        deletedBy: it['audict']['deletedBy']
      },
    });
    console.log(this.eventForm.controls['date'].value)
    this.key = it.id;
    this.uploadedBanner = it['banner'];
    this.modalities = this.eventForm.get('modalities') as FormArray;
    this.modalities.clear();
    it['modalities'].forEach(mod => {
      this.modalities.push(this.fb.group({
        name: mod['name'],
        distance: mod['distance'],
        gender: mod['gender'],
        times: this.fb.array(['03:30 - 04:00', '04:00 - 04:30', '04:30 - 05:00', '05:00 - 05:30', '05:30 - 06:00', '06:00 - 06:30', '06:30 - 07:00', '07:00+']),
      }));
    });
  }

  edit(key: string) {
    this.eventService.getEvent(key).subscribe(it => {
      this.bindFormToModel(it);
      this.ngxSmartModalService.getModal('createEventModal').open();
    });
  }

  view(key: string) {
    this.eventService.getEvent(key).subscribe(it => {
      this.bindFormToModel(it);
      this.viewMode = true;
      this.ngxSmartModalService.getModal('createEventModal').open();
    });
  }

  inactivate(key: string) {
    this.eventService.getEvent(key).subscribe(it => {
      this.event = it as EventModel;
      this.event.status = 'INACTIVE';
      this.eventService.update(this.event, key);
    });
  }

  activate(key: string) {
    this.eventService.getEvent(key).subscribe(it => {
      this.event = it as EventModel;
      this.event.status = 'ACTIVE';
      this.eventService.update(this.event, key);
    });
  }

  ngAfterViewInit() {
    this.ngxSmartModalService.getModal('createEventModal').onAnyCloseEventFinished.subscribe((modal: NgxSmartModalComponent) => {
      this.event = new EventModel();
      this.eventForm.reset();
      this.key = null;
      this.viewMode = false;
      this.createForm();
      modal.removeData();
    });
  }

}
