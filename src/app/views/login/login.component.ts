import { Component } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Router, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user.model';
import { find } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent { 
  loginForm: FormGroup;
  errorMessage: string = '';

  constructor(
    public authService: AuthService,
    private userService: UserService,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.createForm();
  }

  createForm() {
    this.loginForm = this.fb.group({
      email: ['', Validators.required ],
      password: ['',Validators.required]
    });
  }

  tryFacebookLogin(){
    this.authService.doFacebookLogin()
    .then(res => {
      this.router.navigate(['/dashboard']);
    })
  }

  tryTwitterLogin(){
    this.authService.doTwitterLogin()
    .then(res => {
      this.router.navigate(['/dashboard']);
    })
  }

  tryGoogleLogin(){
    this.authService.doGoogleLogin()
    .then(res => {
      this.router.navigate(['/dashboard']);
    })
  }

  tryLogin(value){
    this.authService.doLogin(value)
      .then(res => {
        this.userService.getUserFromDB().subscribe(user => {
          if (user['roles'] !== undefined && user['roles'].indexOf('ADMIN') !== -1) {
            this.router.navigate(['/dashboard']);
          } else {
            console.log('Permission Denied');
            this.errorMessage = 'Permission Denied';
            this.authService.doLogout();
          }
          // this.router.navigate(['/dashboard']);
        });
      }, err => {
        console.log(err);
        this.errorMessage = err.message;
      });
  }
}

