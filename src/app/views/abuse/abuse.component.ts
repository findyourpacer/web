import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AbuseModel } from '../../models/abuse.model';
import { AudictModel } from '../../models/audict.model';
import { User } from '../../models/user.model';
import { AbuseService } from '../../services/abuse.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-abuse',
  templateUrl: 'abuse.component.html',
  styleUrls: ['abuse.component.scss']
})
export class AbuseComponent implements OnInit {
  private abusesSubscription: Subscription;
  private usersSubscription: Subscription;

  abuse: AbuseModel;
  audict: AudictModel;
  abuses: Array<AbuseModel> = [];
  users: Array<User> = [];

  constructor(
    private abuseService: AbuseService,
    private userService: UserService
  ) {
    this.abusesSubscription = this.abuseService.getAllAbuses().subscribe(data => {
      // console.log('abuses => ', data);
      this.abuses = data;
      this.fetchUserAbuses();
    });
    this.usersSubscription = this.userService.getAllUsers().subscribe(data => {
      // console.log('users => ', data);
      this.users = data;
      this.fetchUserAbuses();
    });
  }

  ngOnInit(): void {
    this.abuse = new AbuseModel();
    this.abuse.audict = new AudictModel();
  }

  ngOnDestroy(): void {
    this.abusesSubscription.unsubscribe();
  }

  fetchUserAbuses() {
    if (this.abuses.length > 0 && this.users.length > 0) {
      this.abuses.map(abuse => {
        abuse.userFrom = this.users.find(u => u.id === abuse.from);
        abuse.userTo = this.users.find(u => u.id === abuse.to);
      });
    }
  }
}
