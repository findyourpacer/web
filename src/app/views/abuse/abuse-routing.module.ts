import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AbuseComponent } from './abuse.component';

const routes: Routes = [
  {
    path: '',
    component: AbuseComponent,
    data: {
      title: 'Abuse'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AbuseRoutingModule {}
