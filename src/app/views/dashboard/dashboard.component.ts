import { Component, OnInit } from '@angular/core';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { EventService } from '../../services/event.service';
import { Subscription } from 'rxjs';
import { AbuseService } from '../../services/abuse.service';
import { FeedbackService } from '../../services/feedback.service';
import { UserService } from '../../services/user.service';

@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {
  private eventsSubscription: Subscription;
  private abusesSubscription: Subscription;
  private feedbackSubscription: Subscription;
  private userSubscription: Subscription;
  public eventsNumber: number;
  public abusesNumber: number;
  public feedbacksNumber: number;
  public usersNumber: number;
  public activeEventsNumber: number;
  constructor(
    private eventService: EventService,
    private abuseService: AbuseService,
    private feedbackService: FeedbackService,
    private userService: UserService
  ) {
    this.eventsSubscription = this.eventService.getAllEvents().subscribe(data => {
      this.eventsNumber = data.filter(it => it.status !== 'DELETED').length;
      this.activeEventsNumber = data.filter(it => it.status === 'ACTIVE').length;
    });
    this.abusesSubscription = this.abuseService.getAllAbuses().subscribe(data => {
      this.abusesNumber = data.length;
    });
    this.feedbackSubscription = this.feedbackService.getAllFeedbacks().subscribe(data => {
      this.feedbacksNumber = data.length;
    });
    this.userSubscription = this.userService.getAllUsers().subscribe(data => {
      this.usersNumber = data.length;
    });
  }

  ngOnInit(): void {

  }
}
