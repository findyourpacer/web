import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AudictModel } from '../../models/audict.model';
import { FeedbackModel } from '../../models/feedback.model';
import { User } from '../../models/user.model';
import { FeedbackService } from '../../services/feedback.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-feedback',
  templateUrl: 'feedback.component.html',
  styleUrls: ['feedback.component.scss']
})
export class FeedbackComponent implements OnInit {
  private feedbacksSubscription: Subscription;
  private usersSubscription: Subscription;

  feedback: FeedbackModel;
  audict: AudictModel;
  feedbacks: Array<FeedbackModel> = [];
  users: Array<User> = [];

  constructor(
    private feedbackService: FeedbackService,
    private userService: UserService
  ) {
    this.feedbacksSubscription = this.feedbackService.getAllFeedbacks().subscribe(data => {
      this.feedbacks = data;
      this.fetchUserFeedbacks();
    });
    this.usersSubscription = this.userService.getAllUsers().subscribe(data => {
      // console.log('users => ', data);
      this.users = data;
      this.fetchUserFeedbacks();
    });
  }

  ngOnInit(): void {
    this.feedback = new FeedbackModel();
    this.feedback.audict = new AudictModel();
  }

  ngOnDestroy(): void {
    this.feedbacksSubscription.unsubscribe();
  }

  fetchUserFeedbacks() {
    if (this.feedbacks.length > 0 && this.users.length > 0) {
      this.feedbacks.map(feedback => {
        feedback.userFrom = this.users.find(u => u.id === feedback.from);
      });
    }
  }
}
