import { AudictModel } from './audict.model';
import { User } from './user.model';

export class FeedbackModel {
  id?: string;
  from?: string;
  message?: string;
  created_at?: Date;
  userFrom?: User;
  audict?: AudictModel;
}
