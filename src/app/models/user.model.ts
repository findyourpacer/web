export class FirebaseUserModel {
  image: string;
  name: string;
  provider: string;

  constructor(){
    this.image = "";
    this.name = "";
    this.provider = "";
  }
}

export interface IUser {
  id: string,
  name: string,
  birth_date: string,
  city: string,
  state: string,
  country: string,
  notify: Boolean,
  phone: string,
  profile_picture: string,
  status: string,
  user_name: string,
  chats: Array<any>,
  gender: string,
  role: Array<string>
}

export class User implements IUser {
  id: string = null
  name: string = null
  birth_date: string = null
  city: string= null
  state: string= null
  country: string= null
  notify: Boolean= null
  phone: string= null
  profile_picture: string= null
  status: string= null
  user_name: string= null
  chats: Array<any> = []
  gender: string = null
  role: Array<string> = []
}