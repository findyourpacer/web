import { AudictModel } from './audict.model';
import { User } from './user.model';

export class AbuseModel {
  id?: string;
  from?: string;
  message?: string;
  to?: string;
  userFrom?: User;
  userTo?: User;
  audict?: AudictModel;
}
