import { StringMap } from '@angular/compiler/src/compiler_facade_interface';
import { AudictModel } from './audict.model';

export class EventModel {
  id?: string;
  name?: string;
  date?: Date;
  place?: {
    country?: string;
    state?: string;
    city?: string;
    coordinate?: Coordinates;
  };
  link?: string;
  modalities?: [{
    name?: string;
    distance?: number;
    gender?: string;
    times?: [string];
  }];
  picture?: string;
  registerLink?: string;
  registerPeriod?: {
    begin?: Date;
    end?: Date;
  };
  votes?: [StringMap];
  status?: string;
  banner?: string;
  audict?: AudictModel;
}
