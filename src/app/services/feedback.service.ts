import { Injectable } from "@angular/core";
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import 'firebase/storage';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { FeedbackModel } from '../models/feedback.model';
import { User } from '../models/user.model';
import { UserService } from './user.service';

@Injectable()
export class FeedbackService {
  feedbacks: Array<FeedbackModel>;
  private feedbacksCollention: AngularFirestoreCollection<FeedbackModel>;

  constructor(
    public db: AngularFirestore
  ) {
    this.feedbacksCollention = this.db.collection<FeedbackModel>('feedback');
  }

  getAsyncListOfFeedbacks(): Observable<any> {
    return this.db.collection('feedback').valueChanges();
  }

  getAllFeedbacks() {
    return this.feedbacksCollention.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getFeedback(key: string) {
    return this.feedbacksCollention.doc(key).get().pipe(
      map(a => {
          const data = a.data();
          const id = a.id;
          return { id, ...data };
        })
    );
  }

}
