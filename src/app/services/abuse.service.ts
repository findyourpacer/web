import { Injectable } from "@angular/core";
import 'rxjs/add/operator/toPromise';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import { AbuseModel } from '../models/abuse.model';
import { Observable } from 'rxjs/Observable';
import { map, switchMap, combineLatest } from 'rxjs/operators';
import 'firebase/storage';
import { AudictModel } from '../models/audict.model';
import { UserService } from './user.service';
import { User, FirebaseUserModel } from '../models/user.model';
import { of } from 'rxjs';

@Injectable()
export class AbuseService {
  abuses: Array<AbuseModel>;
  private abusesCollention: AngularFirestoreCollection<AbuseModel>;

  constructor(
    public db: AngularFirestore,
    private userService: UserService
  ) {
    this.abusesCollention = this.db.collection<AbuseModel>('abuse');
  }

  getAsyncListOfAbuses(): Observable<any> {
    return this.db.collection('abuse').valueChanges();
  }

  getAllAbuses() {
    // console.log('#############');
    return this.abusesCollention.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getAbuse(key: string) {
    return this.abusesCollention.doc(key).get().pipe(
      map(a => {
          const data = a.data();
          const id = a.id;

          return { id, ...data };
        })
    );
  }

  mapUserInAbuse(user) {
    return this.userService.getUserByID(user);
  }

}
