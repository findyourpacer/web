import { Injectable } from "@angular/core";
import 'rxjs/add/operator/toPromise';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { User } from '../models/user.model';
import { map } from 'rxjs/operators';

@Injectable()
export class UserService {
  private usersCollention: AngularFirestoreCollection<User>;
  constructor(
    public db: AngularFirestore,
    public afAuth: AngularFireAuth
  ) {
    this.usersCollention = this.db.collection<User>('users');
  }

  getUID(): string {
    return firebase.auth().currentUser.uid;
  }

  getCurrentUser() {
    return new Promise<any>((resolve, reject) => {
      let user = firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
          resolve(user);
        } else {
          reject('No user logged in');
        }
      });
    });
  }

  updateCurrentUser(value) {
    return new Promise<any>((resolve, reject) => {
      let user = firebase.auth().currentUser;
      user.updateProfile({
        displayName: value.name,
        photoURL: user.photoURL
      }).then(res => {
        resolve(res);
      }, err => reject(err));
    });
  }

  getUserFromDB() {
    return this.usersCollention.doc(this.getUID()).get().pipe(
      map(a => {
          const data = a.data();
          const id = a.id;

          return { id, ...data };
        })
    );
  }

  getUserByID(id) {
    return this.usersCollention.doc(id).get().pipe(
      map(a => {
          const data = a.data();
          const id = a.id;
          return { id, ...data };
        })
    );
  }

  getAllUsers() {
    return this.usersCollention.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
}
