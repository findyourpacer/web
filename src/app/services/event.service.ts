import { fcmKey } from './../../environments/environment';
import { Injectable } from "@angular/core";
import 'rxjs/add/operator/toPromise';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { RequestOptions, Headers, Http } from '@angular/http';
import * as firebase from 'firebase/app';
import { EventModel } from './../models/event.model';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import 'firebase/storage';
import { AudictModel } from '../models/audict.model';

@Injectable()
export class EventService {
  events: Array<EventModel>;
  private eventsCollention: AngularFirestoreCollection<EventModel>;

  constructor(
    public db: AngularFirestore,
    private http: Http,
  ) {
    this.eventsCollention = this.db.collection<EventModel>('events');
  }

  getUID(): string {
    return firebase.auth().currentUser.uid;
  }

  insert(event: EventModel) {
    event.status = 'ACTIVE';
    event.audict.createdAt = new Date();
    event.audict.createdBy = this.getUID();
    let newEvent = this.db.collection('events').add(event).then(ref => {
      console.log('Added event with ID: ', ref.id);
      const data = {
        "notification": {
          "title": `Evento ${event.name} cadastrado`,
          "body": "Confira no FYP!",
          "sound": "default",
          "icon": "fcm_push_icon"
        },
        "data": {
          "type": "new-event-added",
          "eventId": ref.id
        },
        "to": "/topics/newEventAdded",
        "priority": "high"
      }
      this.sendNotification(data);
    })
  }

  sendNotification(data) {
    console.log(data)
    let headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'key = ' + fcmKey.key);
    const requestOptions = new RequestOptions({ headers: headers });
    this.http.post("https://fcm.googleapis.com/fcm/send", data, requestOptions).subscribe(data => {
      console.log(data);
      
    }, error => {
      console.log('error')
      console.log(error)
    });
  }

  update(event: EventModel, key: string) {
    event.audict.updatedAt = new Date();
    event.audict.updatedBy = this.getUID();
    this.db.collection('events').doc(key).set(event, { merge: true }).then(ref => {
      console.log(event.name, ' success updated!');
    }).catch((error: any) => {
      console.error(error);
    });
  }


  delete(key: string) {
    this.db.collection('events').doc(key).delete().then(ref => {
      console.log('Success deleted!');
    }).catch((error: any) => {
      console.error(error);
    });
  }

  getAsyncListOfEvents(): Observable<any> {
    return this.db.collection('events').valueChanges();
  }

  getAllEvents() {
    return this.eventsCollention.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getEvent(key: string) {
    return this.eventsCollention.doc(key).get().pipe(
      map(a => {
        const data = a.data();
        const id = a.id;

        return { id, ...data };
      })
    );
  }

  async uploadImage(storage, file) {
    return new Promise((resolve, reject) => {
      try {
        this.uploadToFirebase(storage, file).then(upload => resolve(upload))
      } catch (e) {
        reject()
        console.log("File Upload Error " + e.message);
      }
    })
  }

  uploadToFirebase(storage, file) {
    const JSON = require('circular-json');
    const randomId = Math.random().toString(36).substring(2);
    return new Promise((resolve, reject) => {
      let fileRef = firebase.storage().ref(storage + "/" + randomId + "-" + file.name);

      let uploadTask = fileRef.put(file);

      uploadTask.on(
        "state_changed",
        (_snapshot: any) => {
          console.log(
            "snapshot progess " +
            (_snapshot.bytesTransferred / _snapshot.totalBytes) * 100
          );
        },
        _error => {
          console.log(_error);
          reject(_error);
        },
        () => {
          // completion...
          fileRef.getDownloadURL().then(function (downloadURL) {
            console.log("downloadURL => ", downloadURL);
            resolve(downloadURL);
          });
        }
      );
    });
  }


}
