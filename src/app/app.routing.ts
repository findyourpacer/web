import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import { AuthGuard } from './auth/auth.guard';

export const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full',},
  { path: '404', component: P404Component, data: {title: 'Page 404'}, canActivate: [AuthGuard]},
  { path: '500', component: P500Component, data: {title: 'Page 500'}, canActivate: [AuthGuard]},
  { path: 'login', component: LoginComponent, data: {title: 'Login Page'}, canActivate: [AuthGuard]},
  { path: 'register', component: RegisterComponent, data: {title: 'Register Page'}, canActivate: [AuthGuard]},
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'event',
        loadChildren: () => import('./views/event/event.module').then(m => m.EventModule)
      },
      {
        path: 'abuse',
        loadChildren: () => import('./views/abuse/abuse.module').then(m => m.AbuseModule)
      },
      {
        path: 'feedback',
        loadChildren: () => import('./views/feedback/feedback.module').then(m => m.FeedbackModule)
      }
    ]
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
